//Code javascript tại đây
console.log('Hello cybersoft!');
//Khai báo biến
/*
    var [ten_bien] = [giá_trị]
    =: là phép gán
    Quy tắc đặt tên biến: 
    1 : Rỏ nghĩa (đặt tiếng việt không dấu hoặc tiếng anh)
    2 : Tên biến không được có dấu tiếng việt (telex)
    3 : Tên biến không được có khoảng trống (space)
    4 : Tên biến không được chứa ký tự đặc biệt (!@#$%^&*()+{}|"?><")
    5 : Tên biến không được bắt đầu bằng số
    6 : Đặt tên biến chuẩn camel case 
*/
var number1 = 15;
var ho_ten = 'Nguyễn Văn Tèo';
var hoTen = 'Nguyễn Văn A'; //Chuẩn js
console.log(number1);
console.log(number1);
console.log(number1);
console.log(number1);
/*
   Kiểu dữ liệu cơ sở:
   string : Kiểu chuỗi (Chứa nội dung dài)
   number : Kiểu dữ liệu số học (Thực hiện phép tính toán học)
   boolean: Kiểu dữ liệu đúng sai (true/false)
*/

var title = 'Cybersoft'; //Ví dụ khai báo 1 chuỗi  (string)
var number = 5; // Ví dụ khai báo về số (number)
var flag = false; // Ví dụ khai báo giá trị đúng hoặc sai (true/false) (boolean)

title = 'Hello cybersoft';

console.log(title);
console.log(typeof (title));

/*
    Khai báo hằng số với const (Cũng là biến tương tượng var nhưng không thể gán lại được)
*/

const PI = 3.14;
// PI = 5; fix bug
console.log(PI)


/*
    Toán tử trong javascript
    Phép tính số học: + - * / % (Chia lấy phần dư)
*/
var num1 = 11;
var num2 = 5;
console.log('Tổng là: ', num1 + num2);
console.log('Hiệu là: ', num1 - num2);
console.log('Tích: ', num1 * num2);
console.log('Thương: ', num1 / num2);
console.log('Phép chia dư: ', num1 % num2);

/*
    +: Đối với chuỗi => Nối chuỗi
*/
var loiChao = 'Xin chào ! ';
var ten = 'sideptrai';
console.log(loiChao + ten);

var number1 = '11';
var number2 = '5';
console.log(number1 + number2);

/*
    Phép + dồn giá trị
    Toán tử += : Vừa cộng vừa gán
    ++: 
*/

var tienLuong = 1000;
// tienLuong = tienLuong + 200;
tienLuong += 200;
console.log('Tiền lương: ', tienLuong);

// tienLuong += 1;
tienLuong++; //Tương đương += 1 (Hậu tố)
console.log(tienLuong)


var num4 = 1;
var num5 = num4++; //Hậu tố : Gán sau đó mới tăng
var num6 = ++num4; //Tiền tố: Tăng sau đó mới gán
console.log(num5);
console.log(num6);