//B1: Xác định input của đề bài
var tenPhim = 'Spider man';
var giaVeNguoiLon = 100;
var giaVeTreEm = 80;
var soVeNguoiLon = 1000;
var soVeTreEm = 500;
var phanTramLamTuThien = 10;
//B2: Thực hiện tính toán 
var tongSoVeBanDuoc = soVeNguoiLon + soVeTreEm;
var doanhThu = (soVeTreEm * 80) + (soVeNguoiLon * 100);
var tienTuThien = doanhThu * 10 / 100;
var tienConLai = doanhThu - tienTuThien;
//Bước N: In ra: Tên phim, số vé đã bán, doanh thu, phần trăm từ thiện, tiền từ thiện, tiền còn lại 
console.log('Tên phim: ', tenPhim);
console.log('Số vé đã bán:' , tongSoVeBanDuoc);
console.log('Doanh thu: ', doanhThu);
console.log('Phần trăm từ thiện: ', phanTramLamTuThien);
console.log('Tiền từ thiện: ', tienTuThien);
console.log('Tiền còn lại', tienConLai);